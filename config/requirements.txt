Keras==1.2.0
Theano==0.9.0
cv2==1.0
scikit_learn==0.19b2
numpy>=1.12.1