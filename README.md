# Deep Matching and Validation Network
#### An End-to-End Solution to Constrained Image Splicing Localization and Detection

The provided **python2** (2.7.12) implementation of deep matching and validation network using the **Keras** deep neural network with the **Theano** backend. Detailed python lib dependencies are specified in `config/requirements.txt`.

## Directory Structure
- `config/` 
    - 'keras.json': a sample keras.json config with the Theano backend.
    - 'requirements.txt': a python2 lib env requirement file
- `data/` 
    - `paired_CASIA_ids.csv`: defines the paired CASIA2 dataset
    - `README.md`: step-by-step instruction to prepare CASIA2 dataset
    - `small/`: contains 40 small RGB images from CASIA2
- `expt/` 
    - `test_on_paired_casia/`: contains DMVN prediction results on the paired CASIA2 dataset
- `lib/` 
    - `dmvn/`: DMVN lib
- `model/` 
    - `dmvn_end_to_end.h5`: pretrained DMVN model
- `dmvn_example.ipynb`: ipython notebook of using DMVN to perform image splicing localization and detection using images from `data/small/`
- `dmvn_on_paired_casia.ipynb`: ipython notebook of testing DMVN performance on the paired CASIA2 dataset.
- `README.md`: the current file.

## Usage
Below is a simple code snippet of using the DMVN model to perform splicing localizaiton and detection on a pair of (probe, donor) images.

```python
# load DMVN model and image preprocess
from utils import preprocess_images
from core import create_DMVN_model

# create an end-to-end DMVN model
dmvn_end_to_end = create_DMVN_model()

# load two a DMVN sample of two images
Xp, Xd = preprocess_images( [ probe_file, donor_file ] )
X = { 'world' : Xd, 'probe' : Xp }

# splicing localization and detection via DMVN
pred_masks, pred_probs = dmvn_end_to_end.predict( X )
donor_mask, probe_mask = pred_masks[0]
splicing_prob = pred_probs.ravel()[1]
```

## Contact 
> Dr. Yue Wu

> Email: yue_wu@isi.edu

> Affiliation: USC Information Sciences Institute


## License
The Software is made available for academic or non-commercial purposes only. The license is for a copy of the program for an unlimited term. Individuals requesting a license for commercial use must pay for a commercial license. 

      USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300 
      Los Angeles, CA 90115, USA 
      ATTN: Accounting 

DISCLAIMER. USC MAKES NO EXPRESS OR IMPLIED WARRANTIES, EITHER IN FACT OR BY OPERATION OF LAW, BY STATUTE OR OTHERWISE, AND USC SPECIFICALLY AND EXPRESSLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, VALIDITY OF THE SOFTWARE OR ANY OTHER INTELLECTUAL PROPERTY RIGHTS OR NON-INFRINGEMENT OF THE INTELLECTUAL PROPERTY OR OTHER RIGHTS OF ANY THIRD PARTY. SOFTWARE IS MADE AVAILABLE AS-IS. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT WILL USC BE LIABLE TO ANY USER OF THIS CODE FOR ANY INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES OF ANY KIND, LOST GOODWILL, LOST PROFITS, LOST BUSINESS AND/OR ANY INDIRECT ECONOMIC DAMAGES WHATSOEVER, REGARDLESS OF WHETHER SUCH DAMAGES ARISE FROM CLAIMS BASED UPON CONTRACT, NEGLIGENCE, TORT (INCLUDING STRICT LIABILITY OR OTHER LEGAL THEORY), A BREACH OF ANY WARRANTY OR TERM OF THIS AGREEMENT, AND REGARDLESS OF WHETHER USC WAS ADVISED OR HAD REASON TO KNOW OF THE POSSIBILITY OF INCURRING SUCH DAMAGES IN ADVANCE. 

For commercial license pricing and annual commercial update and support pricing, please contact: 

      Rakesh Pandit USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300
      Los Angeles, CA 90115, USA 

      Tel: +1 213-821-3552
      Fax: +1 213-821-5001 
      Email: rakeshvp@usc.edu and ccto: accounting@stevens.usc.edu
